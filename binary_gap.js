// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

// 100%
function solution(N) {
  const bin = N.toString(2)
  let gap = 0
  let maxgap = 0
  for (var i = 0; i < bin.length; i++) {
    if (bin[i] == 0) {
      gap++
    } else {
      if (gap > maxgap) {
        maxgap = gap
      }
      gap = 0
    }
  }
  return maxgap
  // write your code in JavaScript (Node.js 8.9.4)
}
