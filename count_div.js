// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(A, B, K) {
  // write your code in JavaScript (Node.js 8.9.4)
  const result = []
  for (let i = 0; i < B - A; i++) {
    if ((A + i) % K === 0) {
      result.push(A + i)
    }
  }
  return result.length
}
