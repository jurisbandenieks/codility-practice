// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

// 100%
function solution(A, K) {
  // write your code in JavaScript (Node.js 8.9.4)
  const shift = A
  if (shift.length > 0) {
    for (let i = 0; i < K; i++) {
      const popEl = shift.pop()
      shift.unshift(popEl)
    }
  }

  return shift
}
