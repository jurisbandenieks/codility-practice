// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(A) {
    // write your code in JavaScript (Node.js 8.9.4)
    let myArr=[]
    let AArr = A
    let maxVal = Math.min(...AArr)
    let triplet = 1
    for(let j=0;j<3;j++) {
        for(let i=0;i<AArr.length;i++) {
            if(AArr[i]>maxVal) {
                maxVal=AArr[i]
            }
        }
        var index = AArr.indexOf(maxVal);
        if (index > -1) {
           AArr.splice(index, 1);
        }
        myArr.push(maxVal)
        maxVal = 0
    }
    for(i in myArr) {
        triplet = triplet * myArr[i]
    }
    return triplet
}