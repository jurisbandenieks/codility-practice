function solution(A) {
  let find = 0
  for (let i in A) {
    const index = A.indexOf(A[i])
    const lastIndex = A.lastIndexOf(A[i])

    if (index === lastIndex) {
      find = A[i]
      break
    }
  }
  return find
}
