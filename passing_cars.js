// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(A) {
    var passing = 0
       var east = 0;
       //var west = 0;
       for(var i in A) {
           if(A[i] == 0) {
               east++;
           } else {
               passing+=east;
           }
       }
       if(passing > 1000000000) passing = -1;
       return passing;
   // write your code in JavaScript (Node.js 8.9.4)
}