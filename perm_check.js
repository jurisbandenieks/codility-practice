// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

// 100%
function solution(A) {
  // write your code in JavaScript (Node.js 8.9.4)
  const sorted = A.sort(function(a, b) {
    return a - b
  })

  let find = 1
  for (i in sorted) {
    if (sorted[i] !== find) {
      return 0
    }
    find++
  }
  return 1
}
