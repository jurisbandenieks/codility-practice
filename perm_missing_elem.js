// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(A) {
  // write your code in JavaScript (Node.js 8.9.4)
  let counter = 1
  myArr = A.sort()
  for (i in myArr) {
    if (myArr[i] !== counter) {
      return counter
    }
    counter++
  }
  return counter
}

// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(A) {
  // write your code in JavaScript (Node.js 8.9.4)
  A.sort((a, b) => {
    return a - b
  })
  counter = 1
  while (A[counter - 1] === counter) {
    counter++
  }

  return counter
}
